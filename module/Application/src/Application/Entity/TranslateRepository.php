<?php
namespace Application\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\EntityRepository;

class TranslateRepository extends EntityRepository
{
    public function findInTranslation($params)
    {
        $lang = $params['lang'];
        $name = $params['name'];

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();;

        $qb->select("t")->from('Application\Entity\Translate', "t")
            ->join('t.language', "l");
        $qb->where("l.value = ?1")->setParameter(1, $lang);
        $qb->andWhere("t.translation like ?2")->setParameter(2, "%" . $name . "%");

        $query = $qb->getQuery();
        return $query->getResult();

    }

    public function findByValue($params)
    {
        $lang = $params['lang'];
        $name = $params['name'];

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();;

        $qb->select("t")->from('Application\Entity\Translate', "t")
            ->join('t.language', "l");
        $qb->where("l.value = ?1")->setParameter(1, $lang);
        $qb->andWhere("t.value = ?2")->setParameter(2, $name);

        $query = $qb->getQuery();
        return $query->getResult();

    }
}