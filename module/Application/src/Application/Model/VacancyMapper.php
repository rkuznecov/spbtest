<?php

namespace Application\Model;

use Doctrine\ORM\EntityManager;

class VacancyMapper
{
    protected $manager;

    /**
     * @return \Doctrine\ORM\EntityManager
     */
    public function getManager()
    {
        return $this->manager;
    }

    public function __construct(EntityManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * Ищем модели и возвращаем массив из них
     * @param $params
     *
     * @return VacancyModel[]
     */
    public function searchVacancy($params)
    {
        $vr = $this->getManager()->getRepository('Application\Entity\Vacancy');

        $params['names'] = $this->getMatchedNames($params);
        $res = $vr->findOrderedByName($params);

        $result = array();
        foreach($res as $vacancy) {
            $name = $this->getTranslation($vacancy->getName(), $params['lang']);
            $name = $name ? $name : $vacancy->getName();

            $description = $this->getTranslation($vacancy->getDescription(), $params['lang']);
            $description = $description ? $description : $vacancy->getDescription();

            $result[] = $this->buildModel(array(
                'id' => $vacancy->getId(),
                'name' => $name,
                'description' => $description
            ));
        }

        return $result;
    }

    /**
     * Ф-ия находит подходящие названия в словаре.
     * @param array $params
     * @return array
     */
    private function getMatchedNames($params)
    {
        $tr = $this->getManager()->getRepository('Application\Entity\Translate');

        $res = array();
        $matched = $tr->findInTranslation($params);


        foreach($matched as $name) {
            $res[] = $name->getValue();
        }
        return $res;
    }

    /**
     * Осуществляем перевод строки
     * @param $value
     * @param $lang
     *
     * @return string
     */
    private function getTranslation($value, $lang)
    {
        $name = "";
        $tr = $this->getManager()->getRepository('Application\Entity\Translate');

        $res = $tr->findByValue(array('lang' => $lang, 'name' => $value));
        if (!empty($res)) {
            $name = $res[0]->getTranslation();
        }

        return $name;
    }

    /**
     * Просто заполняем модель
     * @param $data
     *
     * @return VacancyModel
     */
    public function buildModel($data)
    {
        $model = new VacancyModel();
        $model->setId($data['id']);
        $model->setName($data['name']);
        $model->setDescription($data['description']);

        return $model;
    }
}