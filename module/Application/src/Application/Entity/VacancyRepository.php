<?php
namespace Application\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\EntityRepository;

class VacancyRepository extends EntityRepository
{
    /**
     * Ищем модели по критериям
     * @param $params
     *
     * @return array
     */
    public function findOrderedByName($params)
    {
        $dep = $params['dep'];
        $name = $params['name'];
        $names = $params['names'];

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();;
        $qb->select("v")
            ->from('Application\Entity\Vacancy', "v")
            ->join("v.department", "d")
            ->orderBy("v.name", "ASC");

        if ($dep > 0) {
            $qb->andWhere("d.id =?1")->setParameter(1, $dep);
        }

        if (!empty($name)) {
            $qb->andWhere("v.name like ?2")->setParameter(2, "%" . $name . "%");
        }

        if (!empty($names) && is_array($names)) {
            $qb->orWhere("v.name in (?3)")->setParameter(3, $names);
        }

        $query = $qb->getQuery();
        return $query->getResult();

    }
}