$(document).ready(function() {
    $.ajax({
      url: "/application/index/getInitial",
      type: "post",
      success: function(ret) {
        insertInitials(ret);
//        Не допускаем срабатывания клика до подгрузки данных.
        bindEvents();
      }
    });
  }
);

function bindEvents() {
  $("#search-btn").on('click', function() {
    searchVacancy();
  });

  $('#create-db-btn').on('click', function() {
    window.location = "/application/index/fill";
  });
}

/**
 * Поиск вакансий
 */
function searchVacancy() {
  $.ajax({
    url: "/application/index/search",
    type: "post",
    data: getFormValues(),
    success: function(ret) {
      if (ret['data']) {
        insertResults(ret['data']);
      }
    }
  });
}


/**
 * Получаем форму
 * @returns {*|jQuery|HTMLElement}
 */
function getForm() {
  return $('#form-search');
}

/**
 * Получение данных формы.
 * @returns {string}
 */
function getFormValues() {
  return getForm().serialize();
}

/**
 * Рендеринг данных
 * @param {Array} data
 */
function insertResults(data) {
  var tbody = $('#results-table>tbody');
  tbody.empty();

  for (var i = 0; i < data.length; i++) {
    tbody.append('<tr><td></td><td></td></tr>');
    var row = tbody.children('tr:last');
    row.children('td:first').text(data[i].name);
    row.children('td:last').text(data[i].description);
  }
}

/**
 * Вставка данных о языках и отделах изначально.
 * @param {Array} data
 */
function insertInitials(data) {
  if (data) {
    insertInitialLang(data.lang);
    insertInitialDep(data.dep);
  }
}

/**
 * Вствляем языки
 * @param data
 */
function insertInitialLang(data) {
  var lang = getForm().find('select[name="lang"]');
  var needEmptyDefult = true;
  lang.empty();

  for (var i = 0; i < data.length; i++) {
    lang.append('<option></option>');
    var opt = lang.children('option:last');
    opt.attr({
      value: data[i].value
    });
    if (data[i].default) {
      opt.attr({
        selected: 'selected'
      });
      needEmptyDefult = false;
    }
    opt.text(data[i].description);
  }

  if (needEmptyDefult) {
    lang.append('<option selected="selected" value="">Любой</option>');
  }
}

/**
 * Вставляем отделы
 * @param data
 */
function insertInitialDep(data) {
  var deps = getForm().find('select[name="dep"]');
  deps.empty();
  deps.append('<option selected="selected" value="">Любой</option>');
  for (var i = 0; i < data.length; i++) {
    deps.append('<option></option>');
    var opt = deps.children('option:last');
    opt.attr({
      value: data[i].id
    });
    opt.text(data[i].name);
  }
}