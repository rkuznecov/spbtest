<?php
namespace Application\Service;

use Application\Model\VacancyMapper;

use Application\Entity\Departments;
use Application\Entity\Language;
use Application\Entity\Translate;
use Application\Entity\Vacancy as VacancyEnt;

class Vacancy {
    protected $obj_manager;

    public function __construct($om)
    {
        $this->obj_manager = $om;
    }

    /**
     * @return mixed
     */
    public function getObjManager()
    {
        return $this->obj_manager;
    }

    /**
     * Функция возвращает отделы и языки. (не красиво, зато одним запросом)
     * @return array
     */
    public function getInitials()
    {
        return array(
            'dep' => $this->getDeps(),
            'lang' => $this->getLangs()
        );
    }

    /**
     * Вспомогательная ф-ия
     * @return array
     */
    public function getLangs()
    {
        $langs = $this->getObjManager()->getRepository('Application\Entity\Language')->findAll();
        $langs_array = array();
        foreach($langs as $lang) {
            $langs_array[] = array(
                'value' => $lang->getValue(),
                'description' => $lang->getDescription()
            );
        }

        return $langs_array;
    }

    /**
     * Вспомогательная ф-ия
     * @return array
     */
    public function getDeps()
    {
        $deps = $this->getObjManager()->getRepository('Application\Entity\Departments')->findAll();
        $deps_array = array();
        foreach($deps as $dep) {
            $deps_array[] = array(
                'name' => $dep->getName(),
                'id' => $dep->getId()
            );
        }

        return $deps_array;
    }

    /**
     * Основная ф-ия поиска
     * @param array $params
     * @return array
     */
    public function makeSearch($params) {
        $mapper = new VacancyMapper($this->getObjManager());
        $vacancies = $mapper->searchVacancy($params);
        $result = array();
        foreach($vacancies as $vacancy) {
            $result[] = $vacancy->toArray();
        }
        return $result;

    }

    /**
     * Цель метода просто заполнить данными БД. Не стоит рассматривать на предмет кода.
     */
    public function fillData() {
        $em = $this->getObjManager();

        $vacancies = $em->getRepository('Application\Entity\Vacancy')->findAll();

        foreach($vacancies as $vacancy) {
            $em->remove($vacancy);
        }

        $deps = $em->getRepository('Application\Entity\Departments')->findAll();

        foreach($deps as $dep) {
            $em->remove($dep);
        }

        $translates = $em->getRepository('Application\Entity\Translate')->findAll();

        foreach($translates as $tr) {
            $em->remove($tr);
        }

        $langs = $em->getRepository('Application\Entity\Language')->findAll();

        foreach($langs as $lang) {
            $em->remove($lang);
        }
        $em->flush();

        $langEn = new Language();
        $langEn->setValue('en');
        $langEn->setDescription('Eng');
        $em->persist($langEn);

        $langRu = new Language();
        $langRu->setValue('ru');
        $langRu->setDescription('Rus');
        $em->persist($langRu);

        $dep1 = new Departments();
        $dep1->setName("Отдел 1");
        $dep2 = new Departments();
        $dep2->setName("Отдел 2");
        $em->persist($dep1);
        $em->persist($dep2);


        $vac_tr_1 = new Translate();
        $vac_tr_1->setValue("vacancy1");
        $vac_tr_1->setTranslation("Вакансия 1");
        $vac_tr_1->setLanguage($langRu);

        $vac_tr_2 = new Translate();
        $vac_tr_2->setValue("vacancy1 description");
        $vac_tr_2->setTranslation("Вакансия 1 описание");
        $vac_tr_2->setLanguage($langRu);

        $vac_tr_3 = new Translate();
        $vac_tr_3->setValue("vacancy2");
        $vac_tr_3->setTranslation("Вакансия 2");
        $vac_tr_3->setLanguage($langRu);

        $vac_tr_4 = new Translate();
        $vac_tr_4->setValue("vacancy2 description");
        $vac_tr_4->setTranslation("Вакансия 2 описание");
        $vac_tr_4->setLanguage($langRu);

        $em->persist($vac_tr_1);
        $em->persist($vac_tr_2);
        $em->persist($vac_tr_3);
        $em->persist($vac_tr_4);

        $vacancy1 = new VacancyEnt();

        $vacancy1->setName('vacancy1');
        $vacancy1->setDescription("vacancy1 description");
        $vacancy1->setDepartment($dep1);

        $vacancy2 = new VacancyEnt();

        $vacancy2->setName('vacancy2');
        $vacancy2->setDescription("vacancy2 description");
        $vacancy2->setDepartment($dep2);

        $em->persist($vacancy1);
        $em->persist($vacancy2);
        $em->flush();
    }

}