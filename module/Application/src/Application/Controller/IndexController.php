<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

class IndexController extends AbstractActionController
{
    public function indexAction()
    {
        return new ViewModel();
    }

    public function getInitialAction()
    {
        $vs = $this->getServiceLocator()->get('Application\Vacancy');
        $result = array('success' => true);
        try {
            $result = array_merge($result, $vs->getInitials());
        } catch (\Exception $e) {
            $result['success'] = false;
        }

        return new JsonModel($result);
    }

    public function searchAction()
    {
        $request = $this->getRequest();
        $params = array(
            'lang' => $request->getPost('lang'),
            'dep' => $request->getPost('dep'),
            'name' => $request->getPost('search')
        );

        $vs = $this->getServiceLocator()->get('Application\Vacancy');
        $result = array('success' => true);

        try {
            $result['data'] = $vs->makeSearch($params);
        } catch (\Exception $e) {
            $result['success'] = false;
            throw $e;

        }
        return new JsonModel($result);
    }

    /**
     * Экшен нужен только для того, чтобы заполнить талицы тестовыми данными
     * Потому реализация "Чтоб работало" и прошу не оценивать как качество кода.
     */
    public function fillAction()
    {
        $vs = $this->getServiceLocator()->get('Application\Vacancy');
        $vs->fillData();
        $this->redirect()->toUrl("/");
        return new ViewModel();
    }
}
